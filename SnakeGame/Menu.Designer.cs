﻿namespace SnakeGame
{
    partial class FMenu
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.BStart = new System.Windows.Forms.Button();
            this.BEnd = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BStart
            // 
            this.BStart.Location = new System.Drawing.Point(12, 12);
            this.BStart.Name = "BStart";
            this.BStart.Size = new System.Drawing.Size(240, 50);
            this.BStart.TabIndex = 0;
            this.BStart.Text = "スタート";
            this.BStart.UseVisualStyleBackColor = true;
            this.BStart.Click += new System.EventHandler(this.BStart_click);
            // 
            // BEnd
            // 
            this.BEnd.Location = new System.Drawing.Point(12, 68);
            this.BEnd.Name = "BEnd";
            this.BEnd.Size = new System.Drawing.Size(240, 50);
            this.BEnd.TabIndex = 1;
            this.BEnd.Text = "おわる";
            this.BEnd.UseVisualStyleBackColor = true;
            this.BEnd.Click += new System.EventHandler(this.BEnd_click);
            // 
            // FMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(264, 132);
            this.Controls.Add(this.BEnd);
            this.Controls.Add(this.BStart);
            this.Name = "FMenu";
            this.Text = "メニュー";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BStart;
        private System.Windows.Forms.Button BEnd;
    }
}

