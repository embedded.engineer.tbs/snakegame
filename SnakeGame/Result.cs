﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnakeGame
{
    public partial class FResult : Form
    {
        public FResult(string state)
        {
            InitializeComponent();
            this.LResult.Text = state;
        }

        /******************** イベント ********************/
        private void BReplay_click(object sender, EventArgs e)
        {
            Owner.Show();
            Hide();
        }

        private void BEnd_click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void FResult_formClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
        /******************** イベント ********************/
    }
}
