﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace SnakeGame
{
    public partial class FSnakeGame : Form
    {
        public FSnakeGame()
        {
            InitializeComponent();

            // フィールドの大きさに合わせてフォームサイズを調整する
            this.ClientSize = new System.Drawing.Size(20 + 55 * CConstants.WIDTH, 20 + 55 * CConstants.HEIGHT);
            this.PField.Size = new System.Drawing.Size(55 * CConstants.WIDTH, 55 * CConstants.HEIGHT);

            //ResumeLayout()が呼び出されるまで、複数のLayoutイベントが発生しないようにする
            SuspendLayout();

            //PictureBoxをフィールドのマスの分だけ生成する
            for (int i = 0; i < CConstants.WIDTH; i++)
            {
                for (int j = 0; j < CConstants.HEIGHT; j++)
                {
                    // インスタンスの生成
                    m_Pbox[i, j] = new PictureBox();
                    PField.Controls.Add(m_Pbox[i, j]);
                    m_Pbox[i, j].Name = "Pbox[" + i.ToString() + "][" + j.ToString() + "]";
                    m_Pbox[i, j].Location = new Point((55 * i), (55 * j));
                    m_Pbox[i, j].Size = new Size(50, 50);
                }
            }
            ResumeLayout();

            init();
        }

        /******************** イベント ********************/

        // リザルトフォームでリプレイを選択してこのフォームに戻ってきた場合は初期化する
        private void FSnakeGame_activated(object sender, EventArgs e)
        {
            init();
        }

        // フォームを閉じたときに発生
        private void FSnakeGame_formClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        // 一定時間間隔で発火するタイマー（メインループ処理）
        private void T_CycleTimer_tick(object sender, EventArgs e)
        {
            // ヘビを最後に入力されたキーの方向へ進める
            m_snake.move(m_inKeyDir);
            // ヘビが餌や壁に当たったか判定する
            judge();
            // フィールドを描画する
            viewField();
        }

        // キーボードの入力を受け付け、最後に入力されたキーをタイマーが発火するまで記憶する
        private void FSnakeGame_keyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    m_inKeyDir = CConstants.Direction.left;
                    break;
                case Keys.Up:
                    m_inKeyDir = CConstants.Direction.up;
                    break;
                case Keys.Right:
                    m_inKeyDir = CConstants.Direction.right;
                    break;
                case Keys.Down:
                    m_inKeyDir = CConstants.Direction.down;
                    break;
                default:
                    // 何もしない
                    break;
            }
        }
        /******************** イベント ********************/

        private void init()
        {
            m_inKeyDir = CConstants.Direction.left;
            m_snake.initSnake();
            m_food.makeFood(m_snake.getPos(), m_snake.getLength());
            // 壁は最初に1度だけ描画して、以降そのままにしておく
            for (int i = 0; i < CConstants.HEIGHT; i++)
            {
                for (int j = 0; j < CConstants.WIDTH; j++)
                {
                    if (i == 0 || i == (CConstants.HEIGHT - 1) || j == 0 || j == (CConstants.WIDTH - 1)) m_Pbox[i, j].ImageLocation = @"wall.png";
                }
            }
            this.T_CycleTimer.Enabled = true;
        }

        // フィールドを描画する
        private void viewField()
        {
            // フィールドの壁以外を初期化
            for (int i = 0; i < CConstants.WIDTH; i++)
            {
                for (int j = 0; j < CConstants.HEIGHT; j++)
                {
                    // 壁であれば初期化対象から除外する
                    if (i == 0 || i == (CConstants.HEIGHT - 1) || j == 0 || j == (CConstants.WIDTH - 1)) continue;
                    // 壁でなければ初期化する
                    m_Pbox[i, j].Image = null;
                }
            }

            // スネークをフィールドに反映
            for (int i = 0; i < m_snake.getLength(); i++)
            {
                if(i == 0) m_Pbox[m_snake.getBodyX(i), ((CConstants.HEIGHT - 1) - m_snake.getBodyY(i))].ImageLocation = @"SnakeHead.png";
                else m_Pbox[m_snake.getBodyX(i), ((CConstants.HEIGHT - 1) - m_snake.getBodyY(i))].ImageLocation = @"SnakeBody.png";
            }

            // 餌をフィールドに反映
            m_Pbox[m_food.getBodyX(), ((CConstants.HEIGHT - 1) - m_food.getBodyY())].ImageLocation = @"food.png";
        }

        private void judge()
        {
            // 頭が壁に当たったらアウト
            // リザルトフォームを表示する
            if (m_snake.getBodyX(0) == 0 || m_snake.getBodyX(0) == (CConstants.WIDTH - 1) || m_snake.getBodyY(0) == 0 || m_snake.getBodyY(0) == (CConstants.HEIGHT - 1))
            {
                this.T_CycleTimer.Enabled = false;
                FResult f_gameOver = new FResult("Game Over...");
                f_gameOver.Show(this);
                Hide();
            }

            // 頭が体に当たったらアウト
            // リザルトフォームを表示する
            if (m_snake.getLength() < 5);
                // 処理を飛ばす
            else if (m_snake.getLength() >= 5)
            {
                for (int i = 4; i < m_snake.getLength(); i++)
                {
                    if (m_snake.getBodyX(0) == m_snake.getBodyX(i) && m_snake.getBodyY(0) == m_snake.getBodyY(i))
                    {
                        this.T_CycleTimer.Enabled = false;
                        FResult f_gameOver = new FResult("Game Over...");
                        f_gameOver.Show(this);
                        Hide();
                    }
                }
            }

            // 餌を食べた
            if (m_snake.getBodyX(0) == m_food.getBodyX() && m_snake.getBodyY(0) == m_food.getBodyY())
            {
                // ヘビが最大長になったらクリア！
                // リザルトフォームを表示する
                if (m_snake.getLength() == (CConstants.SNAKE_MAX_LENGTH - 1))
                {
                    this.T_CycleTimer.Enabled = false;
                    FResult f_gameOver = new FResult("Clear!!");
                    f_gameOver.Show(this);
                    Hide();
                }
                // 餌を移動する
                m_food.makeFood(m_snake.getPos(), m_snake.getLength());

                // スネークを延ばす
                m_snake.growSnake();
            }
        }

        // メンバ変数
        private PictureBox[,] m_Pbox = new PictureBox[CConstants.WIDTH, CConstants.HEIGHT];
        private CSnake m_snake = new CSnake();
        // 入力されたキーを記憶しておき、タイマーが発火時に進める
        private CConstants.Direction m_inKeyDir;
        private CFood m_food = new CFood();
    }

    class CSnake
    {
        // コンストラクタ
        public CSnake()
        {
            initSnake();
        }

        public void move(CConstants.Direction direction)
        {

            switch (direction)
            {
                case CConstants.Direction.left:
                    //首がある方向のキー入力だった場合は受け付けない
                    if (m_bodyPos[0].x == (m_bodyPos[1].x + 1))
                    {
                        shiftSnake(); // 首から下の座標を頭方向にシフトする
                        m_bodyPos[0].x += 1; // 頭を移動方向へシフトする
                        break;
                    }
                    shiftSnake();
                    m_bodyPos[0].x -= 1;
                    break;
                case CConstants.Direction.up:
                    if (m_bodyPos[0].y == (m_bodyPos[1].y - 1))
                    {
                        shiftSnake();
                        m_bodyPos[0].y -= 1;
                        break;
                    }
                    shiftSnake();
                    m_bodyPos[0].y += 1;
                    break; 
                case CConstants.Direction.right:
                    if (m_bodyPos[0].x == (m_bodyPos[1].x - 1))
                    {
                        shiftSnake();
                        m_bodyPos[0].x -= 1;
                        break;
                    }
                    shiftSnake();
                    m_bodyPos[0].x += 1;
                    break;
                case CConstants.Direction.down:
                    if (m_bodyPos[0].y == (m_bodyPos[1].y + 1))
                    {
                        shiftSnake();
                        m_bodyPos[0].y += 1;
                        break;
                    }
                    shiftSnake();
                    m_bodyPos[0].y -= 1;
                    break;
                default:
                    // 何もしない
                    break;
            }
        }

        // 首から下の座標を頭方向にシフトする
        private void shiftSnake()
        {
            for (int i = 1; i < m_snakeLen; i++)
            {
                m_bodyPos[m_snakeLen - i].x = m_bodyPos[m_snakeLen - (i + 1)].x;
                m_bodyPos[m_snakeLen - i].y = m_bodyPos[m_snakeLen - (i + 1)].y;
            }
        }

        // 体の位置を格納した配列を返す
        public Position[] getPos()
        {
            return m_bodyPos;
        }

        // 体の長さを返す
        public int getLength()
        {
            return m_snakeLen;
        }

        // 指定されたインデックスの体のx座標を返す
        public int getBodyX(int index)
        {
            return m_bodyPos[index].x;
        }

        // 指定されたインデックスの体のy座標を返す
        public int getBodyY(int index)
        {
            return m_bodyPos[index].y;
        }

        // ヘビのしっぽを伸ばす
        public void growSnake()
        {
            m_snakeLen += 1;
            m_bodyPos[m_snakeLen - 1].x = m_bodyPos[m_snakeLen - 2].x;
            m_bodyPos[m_snakeLen - 1].y = m_bodyPos[m_snakeLen - 2].y;
        }

        // ヘビを初期化する
        public void initSnake()
        {
            m_snakeLen = 2;
            // スネークの初期化
            Array.Clear(m_bodyPos, 0, CConstants.SNAKE_MAX_LENGTH);
            m_bodyPos[0].x = CConstants.STARTX;
            m_bodyPos[0].y = CConstants.STARTY;

            m_bodyPos[1].x = CConstants.STARTX + 1;
            m_bodyPos[1].y = CConstants.STARTY;
        }

        // メンバ変数
        private int m_snakeLen;
        private Position[] m_bodyPos = new Position[CConstants.SNAKE_MAX_LENGTH];
    }

    class CFood
    {
        // コンストラクタ
        public CFood()
        {
            m_bodyPos.x = 0;
            m_bodyPos.y = 0;
        }

        // 餌を作る
        public void makeFood(Position[] excludePos, int indexLen)
        {
            // 餌の座標を初期化
            m_bodyPos.x = 0;
            m_bodyPos.y = 0;

            while (true)
            {
                // 餌の座標を１～８でランダムに生成する
                Random r = new Random();
                m_bodyPos.x = 1 + r.Next(CConstants.WIDTH - 3);
                m_bodyPos.y = 1 + r.Next(CConstants.HEIGHT - 3);

                // ヘビの頭からしっぽまでで重なっていないかを確認する
                for (int i = 0; i < indexLen; i++)
                {
                    // ヘビのどこかと重なっていた場合
                    if (excludePos[i].x == m_bodyPos.x && excludePos[i].y == m_bodyPos.y)
                    {
                        // 重なった場所から１マスずらす
                        // 右下角だったら左上角に移す
                        if (m_bodyPos.x == (CConstants.WIDTH - 2) && m_bodyPos.y == (CConstants.HEIGHT - 2))
                        {
                            m_bodyPos.x = 1;
                            m_bodyPos.y = 1;
                        }
                        // 一番右の列だったら一番左の列に移す
                        else if (m_bodyPos.x == CConstants.WIDTH - 2)
                        {
                            m_bodyPos.x = 1;
                            m_bodyPos.y += 1;
                        }
                        // その他の場合は右に一つずらす
                        else
                        {
                            m_bodyPos.x += 1;
                        }
                        // もう一度ヘビの頭から順に重なっていないか確認する
                        i = -1;
                    }
                }
                // 餌がヘビの体のどことも重なっていなければループを抜ける
                break;
            }
        }

        // 餌のx座標を返す
        public int getBodyX()
        {
            return m_bodyPos.x;
        }

        // 餌のy座標を返す
        public int getBodyY()
        {
            return m_bodyPos.y;
        }

        // メンバ変数
        private Position m_bodyPos = new Position();
    }

    // 座標を管理する構造体
    struct Position
    {
        public int x, y;
    }

    // 定数用クラス
    static class CConstants
    {
        // フィールドの横幅と縦幅
        public const int WIDTH = 10;
        public const int HEIGHT = 10;

        // スネークの頭の初期位置
        public const int STARTX = WIDTH / 2;
        public const int STARTY = HEIGHT / 2;

        // スネークの最大長
        public const int SNAKE_MAX_LENGTH = (WIDTH - 2) * (HEIGHT - 2);

        // キー入力用定数
        public enum Direction
        {
            left,
            up,
            right,
            down,
            non
        };
    }
}
