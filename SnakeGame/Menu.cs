﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnakeGame
{
    public partial class FMenu : Form
    {
        public FMenu()
        {
            InitializeComponent();
        
        }

        /******************** イベント ********************/
        private void BStart_click(object sender, EventArgs e)
        {
            FSnakeGame f_SnakeGame = new FSnakeGame();
            f_SnakeGame.Show(this);

            Hide();
        }

        private void BEnd_click(object sender, EventArgs e)
        {
            Close();
        }
        /******************** イベント ********************/
    }
}
