﻿namespace SnakeGame
{
    partial class FResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LResult = new System.Windows.Forms.Label();
            this.BReplay = new System.Windows.Forms.Button();
            this.BEnd = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LResult
            // 
            this.LResult.AutoSize = true;
            this.LResult.Font = new System.Drawing.Font("Meiryo UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LResult.Location = new System.Drawing.Point(98, 28);
            this.LResult.Name = "LResult";
            this.LResult.Size = new System.Drawing.Size(137, 30);
            this.LResult.TabIndex = 0;
            this.LResult.Text = "GameOver";
            this.LResult.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BReplay
            // 
            this.BReplay.Location = new System.Drawing.Point(12, 83);
            this.BReplay.Name = "BReplay";
            this.BReplay.Size = new System.Drawing.Size(150, 50);
            this.BReplay.TabIndex = 1;
            this.BReplay.Text = "リプレイ";
            this.BReplay.UseVisualStyleBackColor = true;
            this.BReplay.Click += new System.EventHandler(this.BReplay_click);
            // 
            // BEnd
            // 
            this.BEnd.Location = new System.Drawing.Point(168, 83);
            this.BEnd.Name = "BEnd";
            this.BEnd.Size = new System.Drawing.Size(150, 50);
            this.BEnd.TabIndex = 2;
            this.BEnd.Text = "おわる";
            this.BEnd.UseVisualStyleBackColor = true;
            this.BEnd.Click += new System.EventHandler(this.BEnd_click);
            // 
            // FResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(330, 145);
            this.Controls.Add(this.BEnd);
            this.Controls.Add(this.BReplay);
            this.Controls.Add(this.LResult);
            this.Name = "FResult";
            this.Text = "リザルト";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FResult_formClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LResult;
        private System.Windows.Forms.Button BReplay;
        private System.Windows.Forms.Button BEnd;
    }
}