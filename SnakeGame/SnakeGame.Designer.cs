﻿namespace SnakeGame
{
    partial class FSnakeGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PField = new System.Windows.Forms.Panel();
            this.T_CycleTimer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // PField
            // 
            this.PField.BackColor = System.Drawing.Color.DarkKhaki;
            this.PField.Location = new System.Drawing.Point(12, 12);
            this.PField.Name = "PField";
            this.PField.Size = new System.Drawing.Size(560, 560);
            this.PField.TabIndex = 0;
            // 
            // T_CycleTimer
            // 
            this.T_CycleTimer.Enabled = true;
            this.T_CycleTimer.Interval = 500;
            this.T_CycleTimer.Tick += new System.EventHandler(this.T_CycleTimer_tick);
            // 
            // FSnakeGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(583, 583);
            this.Controls.Add(this.PField);
            this.Name = "FSnakeGame";
            this.Text = "スネークゲーム";
            this.Activated += new System.EventHandler(this.FSnakeGame_activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FSnakeGame_formClosed);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FSnakeGame_keyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PField;
        private System.Windows.Forms.Timer T_CycleTimer;
    }
}